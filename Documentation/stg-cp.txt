stg-cp(1)
=========
Yann Dirson <ydirson@altern.org>
v0.13, March 2007

NAME
----
stg-cp - stgdesc:cp[]

SYNOPSIS
--------
[verse]
'stg' cp [OPTIONS] <file|dir> <newname>
'stg' cp [OPTIONS] <files|dirs...> <dir>

DESCRIPTION
-----------

Make git-controlled copies of git-controlled files.  The copies are
added to the Git index, so you can add them to a patch with
stglink:refresh[].

In the first form, copy a single file or a single directory, with a
new name.  The parent directory of <newname> must already exist;
<newname> itself must not already exist, or the command will be
interpreted as one of the second form.

In the second form, copy one or several files and/or directories, into
an existing directory.

Directories are copied recursively.  Only the git-controlled files
under the named directories are copied and added to the index.  Any
file not known to Git will not be copied.

CAVEATS
-------

This command does not allow yet to overwrite an existing file (whether
it could be recovered from Git or not).  Further more, when copying a
directory, the second form does not allow to proceed if a directory by
that name already exists inside the target, even when no file inside
that directory would be overwritten.

FUTURE OPTIONS
--------------

No options are supported yet.  The following options may be
implemented in the future.

--all::
	Also copy files not known to Git when copying a directory.

--force::
	Force overwriting of target files, even if overwritten files
	have non-committed changes or are not known to Git.

--dry-run::
	Show which files would be added, and which would be modified
	if --force would be added.

StGIT
-----
Part of the StGIT suite - see gitlink:stg[1].
